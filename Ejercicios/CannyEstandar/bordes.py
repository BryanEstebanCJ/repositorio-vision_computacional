"""import numpy as np
import cv2

src = cv2.imread('./imagenes/image.jpg')

gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (7, 7), 3)

t, dst = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_TRIANGLE)

_, contours, _ = cv2.findContours(dst, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

for c in contours:
    area = cv2.contourArea(c)
    if area > 1000 and area < 10000:
        cv2.drawContours(src, [c], 0, (0, 255, 0), 2, cv2.LINE_AA)

cv2.imshow('contornos', src)
cv2.imshow('umbral', dst)

cv2.waitKey(0)"""
import numpy as np
import cv2 as cv
import glob

def auto_canny(image, sigma=0.5):
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv.Canny(image, lower, upper)

    # return the edged image
    return edged

image = cv.imread('./imagenes/image.jpg')
#imagesRead = [cv.imread(file) for file in glob.glob('./input/*.jpg')]
gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
# blurred = cv.GaussianBlur(gray, (5, 5), 3)
edge = auto_canny(gray)
edges = cv.dilate(edge, None, iterations=1)
cv.imwrite('./bordes/edges.jpg', np.hstack([edges]))
cv.imshow('Edges', edges)
cv.waitKey(0)
cv.destroyAllWindows()