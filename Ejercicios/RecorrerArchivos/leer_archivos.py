from image_read_dir import image_read
import os

print("LEYENDO ARCHIVOS: ")


path = "./dataset1/images_prepped_test"

initSequence = 7959
numSequences = 1
cont_frame = 0
for ns in range(initSequence,numSequences+1):
    cont_frame = cont_frame + 1
    dirimages = path + "/png"
    totalImages = int(len(os.listdir(dirimages)))
    print("Dirimages: "+str(dirimages)+" totalImages: "+str(totalImages))

    image_read(dirimages, totalImages)