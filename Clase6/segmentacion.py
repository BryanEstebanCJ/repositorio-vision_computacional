import cv2 as cv
import numpy as np

img = cv.imread('./imagenes/formas.png')
img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

#Bordes de Canny
edged = cv.Canny(img_gray, 50, 200)
#cv.imshow('1 - Canny Edges', edged)
#cv.waitKey(0)

#Contornos, cuantos se encuentran en la imagen
contours, hierarchy = cv.findContours(edged.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
print("Numero de contornos encontrados: ", len(contours))

#Dibujar los contornos
blank_image = np.zeros(img.shape, np.uint8)
cv.drawContours(blank_image, contours, -1, (0, 255, 0), 3)
cv.imshow('2 - All contours over blank image', blank_image)
cv.imshow('1 - Canny Edges', edged)
cv.waitKey(0)
