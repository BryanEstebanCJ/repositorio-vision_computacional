import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('comic.jpg')
img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
edges = cv.Canny(img, 15, 205)

plt.subplot(121), plt.imshow(img_gray, cmap='gray')
plt.title('Original img'),
plt.xticks([]), plt.yticks([])
plt.subplot(122), plt.imshow(edges, cmap='gray')
plt.title('Edge img'),
plt.xticks([]), plt.yticks([])
plt.show()