import os
import cv2
import matplotlib as plt



pathin="./input"
pathout="./output"
pathout2="/Users/bryancamacho/repos/repositorio-vision_computacional/Clase8/output2"
# image='C:/Users/jennp/PycharmProjects/walk/input/image_00001.jpg'


def programa_finalcanny(pathin):
    totalImagenes = int(len(os.listdir(pathin)))
    print(totalImagenes)
    for nImage in range(0, totalImagenes):
        fileimage = pathin + '/image_' + str(nImage + 1).zfill(5) + '.jpg'
        img = cv2.imread(fileimage)
        edges = cv2.Canny(img, 100, 200)
        cv2.imwrite("./output/" + str(nImage + 1).zfill(5) + '.jpg', edges)


def programa_finalscalegray(pathin):
    totalImagenes = int(len(os.listdir(pathin)))
    print(totalImagenes)
    for nImage in range(0, totalImagenes):
        fileimage = pathin + '/image_' + str(nImage + 1).zfill(5) + '.jpg'
        img = cv2.imread(fileimage)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite("./output2/" + str(nImage + 1).zfill(5) + '.jpg', gray)


if __name__ == "__main__":
    # Extraigo las imagenes a la carpeta Imagespsadasalgoritmo
    # extractImages(pathin,pathout)
    programa_finalcanny(pathin)
    programa_finalscalegray(pathin)